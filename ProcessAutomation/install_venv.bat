REM on fresh start create virtual environment and install required packages

pip install --trusted-host pypi.org virtualenv --ignore-installed

virtualenv venv

if ERRORLEVEL == 0 (
	call venv\Scripts\activate.bat
	pip install --trusted-host pypi.org -r requirement.txt --no-deps --ignore-installed
)

where python
