import os
import glob
import json
import logging

error_list = list()
warning_list = list()

model_fetch_list = None
model_update_list = []

fetch_list = []
update_list = []
file_updated = False
file_valid = False

consolidated_fp = None
common_name_dict = dict()


def report_error(error_msg, print_err=False):
    global error_list

    if error_list is not None and isinstance(error_list, list) and error_msg not in error_list:
        error_list.append(error_msg)

        if print_err:
            logging.error(error_msg)


def report_warning(warn_msg, print_err=False):
    global warning_list

    if parsed_values.ignore_warning is False and \
            warning_list is not None and isinstance(warning_list, list) and warn_msg not in warning_list:

        warning_list.append(warn_msg)
        if print_err:
            logging.warning(warn_msg)


def check_slave_id(type_id, file_base_name):
    global model_fetch_list

    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if file_base_name.find("abb") > 0 and dev_model.get("properties", None) and \
                "slaveID" in dev_model["properties"] and \
                "value" in dev_model["properties"]["slaveID"]:
            slave_id = file_base_name[:file_base_name.find("abb")].strip("_- ")
            slave_id = int(slave_id)
            act_slave_id = int(dev_model["properties"]["slaveID"].get("value", 0))
            if slave_id != act_slave_id:
                report_error(f'file prefix "{slave_id}" does not match with "slaveID" [{act_slave_id}] '
                             f'in {file_base_name}[typeId: {type_id}]')


def update_attributes(type_id):
    global model_fetch_list

    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if not dev_model.get("typeId", None):
            return

        attributes = dev_model.get("attributes", {})

        if "scale" not in attributes:
            attributes["scale"] = {
                "dataType": "number",
                "appliesTo": [
                    "array",
                    "integer",
                    "number",
                    "boolean"
                ]
            }

        if "commonName" not in attributes:
            attributes["commonName"] = {
                "dataType": "string",
                "appliesTo": [
                    "array",
                    "integer",
                    "number",
                    "boolean",
                    "string"
                ]
            }

        if "commonUnit" not in attributes:
            attributes["commonUnit"] = {
                "dataType": "string",
                "appliesTo": [
                    "array",
                    "integer",
                    "number"
                ]
            }

        if "attributes" not in dev_model:
            dev_model["attributes"] = attributes


def get_base_type_ids(type_id):
    model_key = type_id.split("@")[0] if "@" in type_id else type_id

    base_type_id_list = list()
    if model_key in model_fetch_list:
        base_type_id_list = model_fetch_list[model_key].get("baseTypes", list())

        for base_type_id in base_type_id_list:
            base_type_id_list += get_base_type_ids(base_type_id)

    return base_type_id_list


def add_type_id_info(type_id, output_fp=None, output_type=None):
    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if not dev_model.get("typeId", None):
            return

        if output_fp and output_type:
            if output_type == "csv":
                output_fp.write(f'TypeId,LegacyAbilityName,DataType,Description,CommonName,Unit,Remarks\n')


def add_item_info(type_id, item_container, item_key, item_str=None, output_fp=None, output_type=None):
    global file_updated
    global file_valid

    sub_item = item_container[item_key]

    if not item_str:
        item_str = item_key

    if isinstance(sub_item, dict) and \
            all(isinstance(sub_item[sub_item_key], dict) and sub_item_key != "value" for sub_item_key in sub_item):
        for sub_item_key in sub_item:
            add_item_info(type_id=type_id,
                          item_container=sub_item, item_key=sub_item_key, item_str=item_str + "." + sub_item_key,
                          output_fp=output_fp, output_type=output_type)

    else:
        skip_list = [
            "address",
            "protocol",
            "parentId",
            "slaveID",
            "slaveAddress",
            "deviceName",
            "serialNumber"
        ]
        if item_str in skip_list or sub_item.get("disabled", False) is True:
            return

        file_valid = True
        common_name = common_name_dict.get(item_str, "")
        remarks = ""

        if common_name:
            if "commonName" in sub_item:
                if common_name == sub_item["commonName"]:
                    remarks = "commonNameAlready"
                else:
                    remarks = "commonNameChanged"
                    file_updated = True
            else:
                remarks = "commonNameNew"
                file_updated = True

            sub_item["commonName"] = common_name
        else:
            if item_str in common_name_dict.values():
                logging.info(f'"{item_str}" is already a common name')
                common_name = item_str
                remarks = "commonNameIsVariableName"
            else:
                report_warning(f'common name not found for "{item_str}"')
                remarks = "commonNameNotFound"

        if output_fp and output_type:
            if output_type == "csv":
                line_str = f'{type_id},{item_str},{sub_item.get("dataType", "")},' \
                           f'"{sub_item.get("description", "")}",{common_name},{sub_item.get("unit", "")},{remarks}\n'

                output_fp.write(line_str)
                if consolidated_fp:
                    consolidated_fp.write(line_str)


def update_signals(type_id, level_str="base", output_fp=None, output_type=None):
    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if not dev_model.get("typeId", None):
            return

        property_items = dev_model.get("properties", dict())
        if property_items:
            if parsed_values.add_properties:
                for item_key in property_items:
                    add_item_info(type_id=type_id,
                                  item_container=property_items, item_key=item_key,
                                  output_fp=output_fp, output_type=output_type)
            else:
                for item_key in property_items:
                    add_item_info(type_id=type_id,
                                  item_container=property_items, item_key=item_key,
                                  output_fp=None, output_type=None)

        variable_items = dev_model.get("variables", dict())
        if variable_items:
            if parsed_values.add_variables:
                for item_key in variable_items:
                    add_item_info(type_id=type_id,
                                  item_container=variable_items, item_key=item_key,
                                  output_fp=output_fp, output_type=output_type)
            else:
                for item_key in variable_items:
                    add_item_info(type_id=type_id,
                                  item_container=variable_items, item_key=item_key,
                                  output_fp=None, output_type=None)


def increment_version(type_id, version_type):
    global model_fetch_list

    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if "typeId" not in dev_model or "version" not in dev_model:
            report_error(f'vital data missing in {type_id}')
            return

        version = dev_model["version"].split(".")

        if len(version) < 3:
            report_error(f'version [{dev_model["version"]}] is in invalid in {type_id}')
            return

        major = int(version[0])
        minor = int(version[1])
        build = int(version[2])

        if version_type == "major":
            major += 1
        elif version_type == "minor":
            minor += 1
        elif version_type == "build":
            build += 1

        dev_model["version"] = f'{major}.{minor}.{build}'


def update_type_id(model_key, level_str="base", output_fp=None, output_type=None):
    global model_fetch_list

    valid_keys_l1 = [
        "model",
        "typeId",
        "version",
        "name",
        "description",
        "unique",
        "baseTypes",
        "properties",
        "attributes",
        "variables",
        "methods",
        "relatedModels",
        "references"
    ]

    if model_key in model_fetch_list:
        dev_model = model_fetch_list[model_key]
        type_id = dev_model.get("typeId", None)

        if not type_id:
            if not dev_model.get("modelId", None):
                report_error(f'invalid type id. type id "{type_id}" in "{model_key}" is invalid/empty')
            else:
                logging.debug(f"skipping {model_key} as it is a model definition")
            return

        for field_key in dev_model:
            if field_key not in valid_keys_l1:
                report_error(f'invalid key. "{field_key}" in "{type_id}" is not valid."')

        update_signals(type_id=type_id, level_str=level_str,
                       output_fp=output_fp, output_type=output_type)

        if file_updated:
            update_attributes(type_id)
            increment_version(type_id, "minor")


def update_json_files():
    global model_fetch_list

    for json_file in update_list:
        temp_file = json_file + ".tmp"
        base_name_ext = os.path.basename(json_file)
        base_name = os.path.splitext(base_name_ext)[0]

        type_id = base_name[base_name.find("abb"):] if base_name.find("abb") > 0 else base_name

        with open(temp_file, "w") as fp_json_tmp:
            try:
                if type_id in model_fetch_list:
                    json.dump(model_fetch_list[type_id], fp=fp_json_tmp, indent=2, sort_keys=False, ensure_ascii=False)
                else:
                    report_error(f'"{type_id}" not found in parsed model list')
            except Exception as err_msg:
                logging.error(err_msg)
                report_error(str(err_msg))
                continue

        shebang_line = str()
        with open(json_file, "r") as fp_json:
            while True:
                line = fp_json.readline()
                if not line:
                    break
                if "//" in line:
                    shebang_line += line

        try:
            with open(json_file, "w") as fp_json:
                if shebang_line:
                    fp_json.write(shebang_line)

                with open(temp_file, "r") as fp_json_tmp:
                    logging.info("updating " + base_name_ext)
                    while True:
                        line = fp_json_tmp.readline()
                        if not line:
                            break
                        fp_json.write(line)

        except Exception as err_msg:
            report_error(str(err_msg) + ". error occurred while updating " + base_name_ext)
            continue

        os.remove(temp_file)


def fetch_json_files():
    global model_fetch_list

    for json_file in fetch_list:
        base_name_ext = os.path.basename(json_file)
        base_name = os.path.splitext(base_name_ext)[0]

        with open(json_file, "r") as fp:
            try:
                logging.info("parsing " + base_name_ext)

                json_content = str()
                while True:
                    line = fp.readline()
                    if not line:
                        break
                    if "//" not in line:
                        json_content += line.strip()

                info_model = json.loads(json_content)

            except Exception as err_msg:
                if json_file in update_list:
                    report_error(str(err_msg) + ". error occurred while parsing " + base_name_ext)
                else:
                    report_warning(str(err_msg) + ". error occurred while parsing " + base_name_ext)

                continue

            type_id = info_model["typeId"] if "typeId" in info_model else None

            if type_id:
                if json_file in update_list and type_id not in model_update_list:
                    model_update_list.append(type_id)

                if type_id not in model_fetch_list:
                    model_fetch_list[type_id] = info_model
                else:
                    report_error(f'type id redefined. typeId "{type_id}" in "{json_file}" is already used in '
                                 f'another type definition ')

                if type_id not in base_name:
                    report_error("type id and file name mismatch. file_name=" + base_name + ". typeId=" + type_id)

                if type_id in model_update_list:
                    check_slave_id(type_id, base_name)

            else:
                report_error("type id not available in " + base_name_ext)


def replace_if_modified(src_file_path, dst_file_path):
    import filecmp

    if not os.path.exists(src_file_path) or not dst_file_path:
        return

    if os.path.exists(dst_file_path):
        if filecmp.cmp(src_file_path, dst_file_path) is False:
            os.remove(dst_file_path)
            os.rename(src_file_path, dst_file_path)
        else:
            os.remove(src_file_path)
    else:
        os.rename(src_file_path, dst_file_path)


def get_all_fields(field_name):
    result = ""
    field_name_list = field_name.split(".")[:-1]

    for type_id in model_update_list:
        item_container = model_fetch_list[type_id]
        for item_name in field_name_list:
            if item_name not in item_container:
                break
            item_container = item_container[item_name]

        result = result + item_container.get(field_name, "") + ","

    return result


def update_common_name(model_list, output_path=None):
    global consolidated_fp
    global file_updated
    global file_valid

    if not isinstance(model_list, list):
        logging.error("invalid model list")
        return

    if output_path and os.path.exists(output_path) is False:
        os.makedirs(output_path, exist_ok=True)

    output_type = None
    consolidated_file_name = None
    if parsed_values.csv:
        output_type = "csv"
        consolidated_file_name = "consolidated_data.csv"

    if consolidated_file_name:
        consolidated_fp = open(output_path + os.sep + consolidated_file_name, "w") if consolidated_file_name else None
        consolidated_fp.write(f'TypeId,LegacyAbilityName,DataType,Description,CommonName,Unit,Remarks\n')

    for type_id in model_list:
        logging.info("updating common name in " + type_id)

        if output_path:
            temp_file_path = output_path + os.sep + ".temp.txt"
            temp_fp = open(temp_file_path, "w")

            output_file_path = ""
            if output_type == "csv":
                output_file_path = output_path + os.sep + type_id + ".csv"

            add_type_id_info(type_id=type_id, output_fp=temp_fp, output_type=output_type)

            file_updated = False

            update_type_id(model_key=type_id, output_fp=temp_fp, output_type=output_type)

            temp_fp.close()

            if file_valid:
                replace_if_modified(src_file_path=temp_file_path, dst_file_path=output_file_path)

            if os.path.exists(temp_file_path):
                os.remove(temp_file_path)
        else:
            update_type_id(model_key=type_id, output_fp=None)

    if consolidated_fp:
        consolidated_fp.close()


def parse_args():
    import argparse

    parser = argparse.ArgumentParser(description='Update common name in ability IM')

    parser.add_argument(dest="root_dir", metavar="<model root directory>", type=str, help='model root directory path')
    parser.add_argument(dest="cn_csv", metavar="<common name csv>", type=str, help='common name csv')

    parser.add_argument('-ff', "--file-filter", dest="file_filter", type=str, help='file filter ex:*.json')
    parser.add_argument('-fl', "--file-list", dest="file_list", type=str, help='file list')

    parser.add_argument('-b', "--variables", dest="add_variables", action="store_true",
                        help='add variables to output file', default=True)

    parser.add_argument('-p', "--properties", dest="add_properties", action="store_true",
                        help='add properties to output file', default=True)

    parser.add_argument('-iw', "--ignore-warning", dest="ignore_warning",
                        action="store_true", default=False, help='ignores warning')

    parser.add_argument('-ia', "--ignore-attribute", dest="ignore_attribute",
                        action="store_true", default=False, help='ignores checking attribute')

    parser.add_argument('-c', "--csv", dest="csv", action="store_true",
                        help='comma seperated file')

    parser.add_argument('-v', "--verbose", dest="verbose", action="store_true", help='verbosity')

    return parser.parse_args()


def fetch_common_name_csv(csv_file):
    import csv

    with open(csv_file, "r") as fp_csv:
        reader = csv.DictReader(fp_csv)
        for row in reader:
            common_name_dict[row['LegacyAbilityName']] = row['CommonName']


if __name__ == '__main__':
    parsed_values = parse_args()

    try:
        model_fetch_list = dict()
        if parsed_values.verbose:
            logging.basicConfig(level=logging.DEBUG, format="%(asctime)s: %(levelname)s: %(message)s")
        else:
            logging.basicConfig(level=logging.INFO, format="%(asctime)s: %(levelname)s: %(message)s")

        if not parsed_values.root_dir or not os.path.exists(parsed_values.root_dir):
            logging.exception("invalid root directory")
            exit(1)

        if not parsed_values.cn_csv or not os.path.exists(parsed_values.cn_csv):
            logging.exception("invalid common name csv file")
            exit(1)

        root_abs_path = os.path.abspath(parsed_values.root_dir)

        file_query_str = root_abs_path + os.sep + "**\\*.json"
        fetch_list = glob.glob(file_query_str, recursive=True)
        logging.info("fetch file count = " + str(len(fetch_list)))

        if len(fetch_list) == 0:
            logging.error("0 file found for fetching. quiting...")
            exit(1)

        if parsed_values.file_list:
            if ".lst" in parsed_values.file_list:
                with open(parsed_values.file_list, "r") as fp_file_list:
                    while True:
                        line = fp_file_list.readline()
                        if not line:
                            break

                        for file_abs_path in fetch_list:
                            if line.strip("\n ") in file_abs_path:
                                update_list.append(file_abs_path)
            else:
                for file_name in parsed_values.file_list.split(","):
                    file_path = root_abs_path + os.sep + file_name.strip()
                    if file_path not in fetch_list:
                        update_list.append(file_path)
        else:
            if not parsed_values.file_filter:
                parsed_values.file_filter = "*.json"

            file_query_str = root_abs_path + os.sep + "**\\" + parsed_values.file_filter
            update_list = glob.glob(file_query_str, recursive=True)

        logging.info("update file count = " + str(len(update_list)))

        if len(update_list) == 0:
            logging.error("0 file found for common name update. quiting...")
            exit(1)

        fetch_json_files()

        fetch_common_name_csv(parsed_values.cn_csv)

        model_update_list.sort()

        update_output_path = os.path.abspath(root_abs_path + os.sep + "output")

        update_common_name(model_list=model_update_list, output_path=update_output_path)

        update_json_files()

        err_file_log_fp = open(root_abs_path + os.sep + "warnings.log", "w")

        for warn_str in warning_list:
            logging.warning(warn_str)
            err_file_log_fp.write("warning: " + warn_str + "\n")

        for error_str in error_list:
            logging.error(error_str)
            err_file_log_fp.write("error: " + error_str + "\n")

        err_file_log_fp.close()

    except:
        logging.exception("exception")
