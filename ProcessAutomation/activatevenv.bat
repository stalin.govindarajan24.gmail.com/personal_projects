@echo off
IF EXIST venv (
	call venv\Scripts\activate.bat
)

where python
set PYTHONPATH=%CD%
