import os
import glob
import json
import logging
from os.path import basename

from setuptools.command.rotate import rotate

error_list = list()
warning_list = list()

model_fetch_list = None
model_analyse_list = []

prefix_skip = "abb.ability."
fetch_list = []
analyse_list = []
item_info_count = 0

type_id_signals = {}

consolidated_signals = {
    "signals": {},
    "type_ids": {}
}

consolidated_fp = None


def report_error(error_msg, print_err=False):
    global error_list

    if error_list is not None and isinstance(error_list, list) and error_msg not in error_list:
        error_list.append(error_msg)

        if print_err:
            logging.error(error_msg)


def report_warning(warn_msg, print_err=False):
    global warning_list

    if parsed_values.ignore_warning is False and \
            warning_list is not None and isinstance(warning_list, list) and warn_msg not in warning_list:

        warning_list.append(warn_msg)
        if print_err:
            logging.warning(warn_msg)


def check_attribute(attribute, type_id):
    if type_id not in model_fetch_list:
        return False

    dev_model = model_fetch_list[type_id]
    result = False

    if attribute not in ["dataType", "value", "items", "values", "description", "unit",
                         "min", "max", "enum", "isMandatory"] and \
            attribute not in dev_model.get("attributes", {}):
        if len(dev_model.get("baseTypes", {})):
            for base_type in dev_model.get("baseTypes", {}):
                base_type = base_type.split("@")[0]
                result = check_attribute(attribute, base_type)
                if result is True:
                    break
        else:
            result = False
    else:
        result = True

    return result


def check_slave_id(type_id, file_base_name):
    global model_fetch_list

    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if file_base_name.find("abb") > 0 and dev_model.get("properties", None) and \
                "slaveID" in dev_model["properties"] and \
                "value" in dev_model["properties"]["slaveID"]:
            slave_id = file_base_name[:file_base_name.find("abb")].strip("_- ")
            slave_id = int(slave_id)
            act_slave_id = int(dev_model["properties"]["slaveID"].get("value", 0))
            if slave_id != act_slave_id:
                report_error(f'file prefix "{slave_id}" does not match with "slaveID" [{act_slave_id}] '
                             f'in {file_base_name}[typeId: {type_id}]')


def check_attributes(type_id):
    global model_fetch_list

    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if not dev_model.get("typeId", None):
            return

        if dev_model.get("properties", None):
            for item_key in dev_model.get("properties", {}):
                for attribute in dev_model["properties"][item_key]:
                    if not check_attribute(attribute, type_id):
                        report_error(f'attribute "{attribute}" of property "{item_key}" in {type_id} is undefined')

        if dev_model.get("variables", None):
            for item_key in dev_model.get("variables", {}):
                for attribute in dev_model["variables"][item_key]:
                    if not check_attribute(attribute, type_id):
                        report_error(f'attribute "{attribute}" of variable "{item_key}"  in {type_id} is undefined')


def check_duplicate_properties(type_id, type_id_1, type_id_2):
    global model_fetch_list

    info_model_1 = model_fetch_list.get(type_id_1, dict())
    info_model_2 = model_fetch_list.get(type_id_2, dict())

    if "properties" in info_model_1 and "properties" in info_model_2:
        model_properties_1 = info_model_1["properties"]
        model_properties_2 = info_model_2["properties"]

        for property_item in model_properties_1:
            if property_item in model_properties_2:
                for property_attribute in model_properties_1[property_item]:
                    if property_attribute in model_properties_2[property_item] and property_attribute != "value":
                        if model_properties_1[property_item][property_attribute] != \
                                model_properties_2[property_item][property_attribute]:
                            report_error(f'value of attribute "{property_attribute}" of property "{property_item}" '
                                         f'is different in '
                                         f'"{type_id_1}[{model_properties_1[property_item][property_attribute]}"] and '
                                         f'"{type_id_2}[{model_properties_2[property_item][property_attribute]}"] '
                                         f'in base types hierarchy of "{type_id}"')
                        else:
                            report_warning(f'attribute "{property_attribute}" of property "{property_item}" is defined '
                                           f'in "{type_id_1}" and "{type_id_2}" in base types of "{type_id}"')


def check_duplicate_variables(type_id, type_id_1, type_id_2):
    global model_fetch_list

    info_model_1 = model_fetch_list.get(type_id_1, dict())
    info_model_2 = model_fetch_list.get(type_id_2, dict())

    if "variables" in info_model_1 and "variables" in info_model_2:
        model_properties_1 = info_model_1["variables"]
        model_properties_2 = info_model_2["variables"]

        for variable_item in model_properties_1:
            if variable_item in model_properties_2:
                for variable_attribute in model_properties_1[variable_item]:
                    if variable_attribute in model_properties_2[variable_item] and variable_attribute != "value":
                        if model_properties_1[variable_item][variable_attribute] != \
                                model_properties_2[variable_item][variable_attribute]:
                            report_error(f'value of attribute "{variable_attribute}" of variable "{variable_item}" '
                                         f'is different in '
                                         f'"{type_id_1}"["{model_properties_1[variable_item][variable_attribute]}"] and '
                                         f'"{type_id_2}"["{model_properties_2[variable_item][variable_attribute]}"] '
                                         f'in base types hierarchy of "{type_id}"')
                        else:
                            report_warning(f'attribute "{variable_attribute}" of variable "{variable_item}" is defined '
                                           f'in "{type_id_1}" and "{type_id_2}" in base types of "{type_id}"')


def check_duplicate_attributes(type_id, type_id_1, type_id_2):
    global model_fetch_list

    info_model_1 = model_fetch_list.get(type_id_1, dict())
    info_model_2 = model_fetch_list.get(type_id_2, dict())

    if "attributes" in info_model_1 and "attributes" in info_model_2:
        model_attributes_1 = info_model_1["attributes"]
        model_attributes_2 = info_model_2["attributes"]

        for item in model_attributes_1:
            if item in model_attributes_2:
                for attributes_attribute in model_attributes_1[item]:
                    if attributes_attribute in model_attributes_2[item]:
                        if model_attributes_1[item][attributes_attribute] != \
                                model_attributes_2[item][attributes_attribute]:
                            report_error(f'value of "{attributes_attribute}" of attributes "{item}" '
                                         f'is different in '
                                         f'"{type_id_1}[{model_attributes_1[item][attributes_attribute]}"] and '
                                         f'"{type_id_2}[{model_attributes_2[item][attributes_attribute]}"] '
                                         f'in base types hierarchy of "{type_id}"')
                        else:
                            report_warning(f'attribute item "{attributes_attribute}" of attributes "{item}" is defined '
                                           f'in "{type_id_1}" and "{type_id_2}" in base types of "{type_id}"')


def get_base_type_ids(type_id):
    model_key = type_id.split("@")[0] if "@" in type_id else type_id

    base_type_id_list = list()
    if model_key in model_fetch_list:
        base_type_id_list = model_fetch_list[model_key].get("baseTypes", list())

        for base_type_id in base_type_id_list:
            base_type_id_list += get_base_type_ids(base_type_id)

    return base_type_id_list


def add_type_id_info(type_id, output_fp=None, output_type=None):
    global prefix_skip

    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if not dev_model.get("typeId", None):
            return

        if output_fp and output_type:
            if output_type == "uml":
                output_fp.write(f'class {dev_model.get("typeId", "").replace(prefix_skip, "")}\n')

                output_fp.write("note as N1" + "\n")
                output_fp.write("\t<b>model:</b> " + dev_model.get("model", "") + "\n")
                output_fp.write("\t<b>typeId:</b> " + dev_model.get("typeId", "") + "\n")
                output_fp.write("\t<b>version:</b> " + dev_model.get("version", "") + "\n")

                if dev_model.get("name"):
                    output_fp.write("\t<b>name:</b> " + dev_model.get("name") + "\n")

                if dev_model.get("description"):
                    output_fp.write("\t<b>description:</b> " + dev_model.get("description") + "\n")

                output_fp.write("end note" + "\n")

            elif output_type == "markdown":
                output_fp.write("\n")
                output_fp.write(f'# {dev_model.get("name", "")} ({type_id})\n')
                output_fp.write("## Type Id Info:\n")
                output_fp.write("- **model:** " + dev_model.get("model", "") + "\n")
                output_fp.write("- **typeId:** " + dev_model.get("typeId", "") + "\n")
                output_fp.write("- **version:** " + dev_model.get("version", "") + "\n")
                output_fp.write("- **name:** " + dev_model.get("name", "") + "\n")

                if dev_model.get("description"):
                    output_fp.write("- **description:** " + dev_model.get("description") + "\n")

            elif output_type == "csv":
                output_fp.write(f'typeID,variableName,description,readAddress,value,bitfield,regType,dataType,'
                                f'registryCount,min,max,unit,scale,pollingType,pollingTypes,targetDataType,'
                                f'invalidValue,tpName,forceValue\n')


def add_item_info(type_id, item_container, item_key, item_str=None, output_fp=None, output_type=None):
    global item_info_count
    global type_id_signals

    sub_item = item_container[item_key]

    if not item_str:
        item_str = item_key

    if isinstance(sub_item, dict) and \
            all(isinstance(sub_item[sub_item_key], dict) and sub_item_key != "value" for sub_item_key in sub_item):
        for sub_item_key in sub_item:
            add_item_info(type_id=type_id,
                          item_container=sub_item, item_key=sub_item_key, item_str=item_str + "." + sub_item_key,
                          output_fp=output_fp, output_type=output_type)
    else:
        signal_data = {
            "dataType": sub_item.get("dataType", None),
            "unit": sub_item.get("unit", None),
            "value": sub_item.get("value", None),
            "readAddress": sub_item.get("readAddress", None),
            "regType": sub_item.get("regType", None),
            "scale": sub_item.get("scale", None),
            "pollingType": sub_item.get("pollingType", None),
            "targetDataType": sub_item.get("targetDataType", None),
            "bitfield": sub_item.get("bitfield", None),
            "description": sub_item.get("description", None),
            "name": item_str
        }

        if item_key not in type_id_signals:
            type_id_signals[item_key] = signal_data
        else:
            type_id_signals[item_key].update(signal_data)

        if output_fp and output_type:
            if output_type == "uml":
                output_fp.write(f'\t{item_str}: {sub_item.get("dataType")}\n')

            elif output_type == "markdown":
                output_fp.write(f'| {item_info_count} | {item_str} | {sub_item.get("dataType")} '
                                f'| {sub_item.get("description", "")} |\n')
                item_info_count = item_info_count + 1

            elif output_type == "csv":
                # (f'typeID,variableName,description,readAddress,value,bitfield,regType,dataType,'
                # f'registryCount,min,max,unit,scale,pollingType,pollingTypes,targetDataType,'
                # f'invalidValue,tpName,forceValue\n')

                line_str = f'{type_id},' \
                           f'{item_str},' \
                           f'"{sub_item.get("description", "")}",' \
                           f'{sub_item.get("readAddress", "")},' \
                           f'{sub_item.get("value", "")},' \
                           f'{sub_item.get("bitfield", "")},' \
                           f'{sub_item.get("regType", "")},' \
                           f'{sub_item.get("dataType", "")},' \
                           f'{sub_item.get("registryCount", "")},' \
                           f'{sub_item.get("min", "")},' \
                           f'{sub_item.get("max", "")},' \
                           f'{sub_item.get("unit", "")},' \
                           f'{sub_item.get("scale", "")},' \
                           f'{sub_item.get("pollingType", "")},' \
                           f'{sub_item.get("pollingTypes", "")},' \
                           f'{sub_item.get("targetDataType", "")},' \
                           f'{sub_item.get("invalidValue", "")},' \
                           f'{sub_item.get("tpName", "")},' \
                           f'{sub_item.get("forceValue", "")},' \
                           f'\n'

                if consolidated_fp:
                    consolidated_fp.write(line_str)
                output_fp.write(line_str)


def add_type_id_class_details(type_id, level_str="base", level_count=1, output_fp=None, output_type=None):
    global prefix_skip
    global item_info_count

    if type_id in model_fetch_list:
        dev_model = model_fetch_list[type_id]

        if not dev_model.get("typeId", None):
            return

        if output_fp and output_type:
            if output_type == "uml":
                output_fp.write(f'class {dev_model.get("typeId", "").replace(prefix_skip, "")} {{\n')

            elif output_type == "markdown":
                if level_count > 1:
                    return

            if all(item == "baseTypes" for item in level_str.split(".")[1:]):
                property_items = dev_model.get("properties", dict())
                if property_items:
                    if parsed_values.add_properties:
                        if output_type == "uml":
                            output_fp.write("..properties..\n")

                        elif output_type == "markdown":
                            output_fp.write("\n\n" + "#" * (level_count + 1) + " Properties:\n")
                            output_fp.write("| S.No | Name | Data Type | Description |\n")
                            output_fp.write("| :---: | :---: | :---: | :---: |\n")
                            item_info_count = 1

                        for item_key in property_items:
                            add_item_info(type_id=type_id,
                                          item_container=property_items, item_key=item_key,
                                          output_fp=output_fp, output_type=output_type)
                    else:
                        for item_key in property_items:
                            add_item_info(type_id=type_id,
                                          item_container=property_items, item_key=item_key,
                                          output_fp=None, output_type=None)

                variable_items = dev_model.get("variables", dict())
                if variable_items:
                    if parsed_values.add_variables:
                        if output_type == "uml":
                            output_fp.write("..variables..\n")

                        elif output_type == "markdown":
                            output_fp.write("\n\n" + "#" * (level_count + 1) + " Variables:\n")
                            output_fp.write("| S.No | Name | Data Type | Description |\n")
                            output_fp.write("| :---: | :---: | :---: | :---: |\n")
                            item_info_count = 1

                        for item_key in variable_items:
                            add_item_info(type_id=type_id,
                                          item_container=variable_items, item_key=item_key,
                                          output_fp=output_fp, output_type=output_type)
                    else:
                        for item_key in variable_items:
                            add_item_info(type_id=type_id,
                                          item_container=variable_items, item_key=item_key,
                                          output_fp=None, output_type=None)

            if output_type == "uml":
                output_fp.write(f'}}\n')


def analyze_type_id(model_key, level_str="base", level_count=1, output_fp=None, output_type=None):
    global model_fetch_list
    global prefix_skip

    valid_keys_l1 = [
        "model",
        "typeId",
        "version",
        "name",
        "description",
        "unique",
        "baseTypes",
        "properties",
        "attributes",
        "variables",
        "methods",
        "relatedModels",
        "references"
    ]

    if model_key in model_fetch_list:
        dev_model = model_fetch_list[model_key]
        type_id = dev_model.get("typeId", None)

        if not type_id:
            if not dev_model.get("modelId", None):
                report_error(f'invalid type id. type id "{type_id}" in "{model_key}" is invalid/empty')
            else:
                logging.debug(f"skipping {model_key} as it is a model definition")
            return

        for field_key in dev_model:
            if field_key not in valid_keys_l1:
                report_error(f'invalid key. "{field_key}" in "{type_id}" is not valid."')

        if output_type == "uml" or output_type == "markdown":
            add_type_id_class_details(type_id=type_id,
                                      level_str=level_str, level_count=level_count,
                                      output_fp=output_fp, output_type=output_type)

        if not parsed_values.ignore_attribute_check:
            check_attributes(type_id)

        for field_key in ["baseTypes", "references", "relatedModels"]:
            assoc_type_id_list = dict()

            if field_key in dev_model and len(dev_model[field_key]) > 0:
                if parsed_values.base_types >= level_count and field_key == "baseTypes":
                    item_count = 1
                    for item_value in dev_model["baseTypes"]:
                        assoc_type_id_list[item_count] = item_value
                        item_count = item_count + 1

                elif parsed_values.related_models >= level_count and field_key == "relatedModels":
                    for item_key in dev_model["relatedModels"]:
                        rel_model_field = dev_model["relatedModels"][item_key]
                        assoc_type_id_list[item_key] = rel_model_field["type"]

                elif parsed_values.reference_models >= level_count and field_key == "references":
                    for item_key in dev_model["references"]:
                        ref_field = dev_model["references"][item_key]
                        if "to" in ref_field and len(ref_field["to"]) > 0:
                            if len(ref_field["to"]) > 1:
                                item_count = 1
                                for ref_type in ref_field["to"]:
                                    assoc_type_id_list[f'{item_key}.{item_count}'] = ref_type["type"]
                                    item_count = item_count + 1
                            else:
                                assoc_type_id_list[item_key] = ref_field["to"][0]["type"]

                association_str = None
                association_text = None

                if output_fp and len(assoc_type_id_list) > 0:
                    if output_type == "uml":
                        if field_key == "baseTypes":
                            association_str = " <|-[thickness=1]- "
                        elif field_key == "relatedModels":
                            association_str = " <-[#blue,dashed,thickness=2] "
                        elif field_key == "references":
                            association_str = " -[#SeaGreen,thickness=2]-* "

                    elif output_type == "markdown":
                        subtitle_str = None

                        if field_key == "baseTypes":
                            subtitle_str = " Base Type(s)"
                        elif field_key == "relatedModels":
                            subtitle_str = " Related Models"
                        elif field_key == "references":
                            subtitle_str = " Reference Models"

                        if subtitle_str:
                            output_fp.write("\n\n" + "#" * (level_count + 1) + subtitle_str)

                for assoc_type_id_key in assoc_type_id_list:
                    assoc_type_id = assoc_type_id_list[assoc_type_id_key]
                    assoc_type_id_split = assoc_type_id.split("@")
                    assoc_type_id = assoc_type_id_split[0]

                    assoc_type_id_ver = ""
                    if len(assoc_type_id_split) > 1:
                        assoc_type_id_ver = assoc_type_id_split[1]

                        if field_key == "baseTypes":
                            if len(assoc_type_id_ver.split(".")) < 3:
                                report_error(f'invalid version format. version information in '
                                             f'type id "{assoc_type_id}" under {field_key} is invalid. '
                                             f'format shall be "@major.minor.patch"')
                        else:
                            if len(assoc_type_id_ver.split(".")) > 1:
                                report_error(f'invalid version format. version information in '
                                             f'type id "{assoc_type_id}" under {field_key} is invalid. '
                                             f'format shall be "@major"')

                    else:
                        report_error(f'version not given. version information is not specified for '
                                     f'type id "{assoc_type_id}" under {field_key} in "{type_id}"')

                    if assoc_type_id in model_fetch_list:
                        if assoc_type_id_ver:
                            version = model_fetch_list[assoc_type_id].get("version", "")
                            if assoc_type_id_ver != version[0:len(assoc_type_id_ver)]:
                                report_error(f'version mismatch. version of type id "{assoc_type_id}" under '
                                             f'{field_key} in "{type_id}" is {assoc_type_id_ver}. '
                                             f'actual version is {version}')
                    else:
                        report_error(f'type id unavailable. type id "{assoc_type_id}" under {field_key} in '
                                     f'"{type_id}" is unavailable')
                        if output_fp and output_type:
                            if output_type == "uml":
                                output_fp.write(f'class {assoc_type_id.replace(prefix_skip, "")} #back:red\n')

                    if output_fp:
                        if output_type == "uml":
                            if field_key == "references":
                                association_text = assoc_type_id_key
                                if dev_model[field_key][assoc_type_id_key].get("isContainment", False) is True:
                                    association_str = " -[#SeaGreen,thickness=2]-* "
                                else:
                                    association_str = " -[#SeaGreen,thickness=2]-o "

                            if association_str:
                                uml_str = assoc_type_id.replace(prefix_skip, "") + association_str + \
                                          type_id.replace(prefix_skip, "")

                            if association_text:
                                uml_str = uml_str + ": " + association_text

                            output_fp.write(uml_str + "\n")

                            logging.debug(uml_str)

                        elif output_type == "markdown":
                            output_fp.write("\n")
                            output_fp.write(f'- [{assoc_type_id}]({assoc_type_id}.html)\n')

                    if field_key == "baseTypes":
                        check_duplicate_properties(type_id=type_id, type_id_1=type_id, type_id_2=assoc_type_id)
                        check_duplicate_variables(type_id=type_id, type_id_1=type_id, type_id_2=assoc_type_id)
                        check_duplicate_attributes(type_id=type_id, type_id_1=type_id, type_id_2=assoc_type_id)

                    next_level_str = level_str + "." + field_key
                    analyze_type_id(model_key=assoc_type_id,
                                    level_str=next_level_str, level_count=level_count + 1,
                                    output_fp=output_fp, output_type=output_type)

                # check for conflicting attributes
                if field_key == "baseTypes":
                    base_type_id_list = [type_id]
                    base_type_id_list += assoc_type_id_list.values()

                    for base_type_id in assoc_type_id_list.values():
                        base_type_id_list += get_base_type_ids(base_type_id)

                    item_count_1 = 0
                    while item_count_1 < len(base_type_id_list):
                        type_id_1 = base_type_id_list[item_count_1].split("@")[0]

                        item_count_2 = item_count_1 + 1
                        while item_count_2 < len(base_type_id_list):
                            type_id_2 = base_type_id_list[item_count_2].split("@")[0]
                            if type_id_1 != type_id_2:
                                check_duplicate_properties(type_id=type_id, type_id_1=type_id_1, type_id_2=type_id_2)
                                check_duplicate_variables(type_id=type_id, type_id_1=type_id_1, type_id_2=type_id_2)
                                check_duplicate_attributes(type_id=type_id, type_id_1=type_id_1, type_id_2=type_id_2)

                            item_count_2 = item_count_2 + 1

                        item_count_1 = item_count_1 + 1

        if output_type == "csv":
            add_type_id_class_details(type_id=type_id,
                                      level_str=level_str, level_count=level_count,
                                      output_fp=output_fp, output_type=output_type)


def format_all_models():
    global model_fetch_list

    for json_file in analyse_list:
        temp_file = json_file + ".tmp"
        base_name_ext = os.path.basename(json_file)
        base_name = os.path.splitext(base_name_ext)[0]

        type_id = base_name[base_name.find("abb"):] if base_name.find("abb") > 0 else base_name

        with open(temp_file, "w") as fp_json_tmp:
            try:
                if type_id in model_fetch_list:
                    json.dump(model_fetch_list[type_id], fp=fp_json_tmp, indent=2, sort_keys=False, ensure_ascii=False)
                else:
                    report_error(f'"{type_id}" not found in parsed model list')
            except Exception as err_msg:
                logging.error(err_msg)
                report_error(str(err_msg))
                continue

        shebang_line = str()
        with open(json_file, "r") as fp_json:
            while True:
                line = fp_json.readline()
                if not line:
                    break
                if "//" in line:
                    shebang_line += line

        try:
            with open(json_file, "w") as fp_json:
                if shebang_line:
                    fp_json.write(shebang_line)

                with open(temp_file, "r") as fp_json_tmp:
                    logging.info("reformatting " + base_name_ext)
                    while True:
                        line = fp_json_tmp.readline()
                        if not line:
                            break
                        fp_json.write(line)

        except Exception as err_msg:
            report_error(str(err_msg) + ". error occurred while reformatting " + base_name_ext)
            continue

        os.remove(temp_file)


def fetch_all_models():
    global model_fetch_list

    for json_file in fetch_list:
        base_name_ext = os.path.basename(json_file)
        base_name = os.path.splitext(base_name_ext)[0]

        with open(json_file, "r") as fp:
            try:
                logging.info("parsing " + base_name_ext)

                json_content = str()
                while True:
                    line = fp.readline()
                    if not line:
                        break
                    if "//" not in line:
                        json_content += line.strip()

                info_model = json.loads(json_content)

            except Exception as err_msg:
                if json_file in analyse_list:
                    report_error(str(err_msg) + ". error occurred while parsing " + base_name_ext)
                else:
                    report_warning(str(err_msg) + ". error occurred while parsing " + base_name_ext)

                continue

            type_id = info_model["typeId"] if "typeId" in info_model else None

            if type_id:
                if json_file in analyse_list and type_id not in model_analyse_list:
                    model_analyse_list.append(type_id)

                if type_id not in model_fetch_list:
                    model_fetch_list[type_id] = info_model
                else:
                    report_error(f'type id redefined. typeId "{type_id}" in "{json_file}" is already used in '
                                 f'another type definition ')

                if type_id not in base_name:
                    report_error("type id and file name mismatch. file_name=" + base_name + ". typeId=" + type_id)

                if type_id in model_analyse_list:
                    check_slave_id(type_id, base_name)

            else:
                report_error("type id not available in " + base_name_ext)


def replace_if_modified(src_file_path, dst_file_path):
    import filecmp

    if not os.path.exists(src_file_path) or not dst_file_path:
        return

    if os.path.exists(dst_file_path):
        if filecmp.cmp(src_file_path, dst_file_path) is False:
            os.remove(dst_file_path)
            os.rename(src_file_path, dst_file_path)
        else:
            os.remove(src_file_path)
    else:
        os.rename(src_file_path, dst_file_path)


def finalize_uml_file(src_file_path, dst_file_path):
    import hashlib

    temp_output_file = dst_file_path + ".tmp"
    completed_lines_hash = set()
    output_file = open(temp_output_file, "w")

    for line in open(src_file_path, "r"):
        if any(item in line for item in ["<", "*", "o-", "-o"]):
            hash_value = hashlib.md5(line.rstrip().encode('utf-8')).hexdigest()
            if hash_value not in completed_lines_hash:
                output_file.write(line)
                completed_lines_hash.add(hash_value)
        else:
            output_file.write(line)

    output_file.close()

    replace_if_modified(src_file_path=temp_output_file, dst_file_path=dst_file_path)


def get_all_fields(field_name):
    result = ""
    field_name_list = field_name.split(".")[:-1]

    for type_id in model_analyse_list:
        item_container = model_fetch_list[type_id]
        for item_name in field_name_list:
            if item_name not in item_container:
                break
            item_container = item_container[item_name]

        result = result + item_container.get(field_name, "") + ","

    return result


def signal_presence_status(signal_name, fill_str):
    global consolidated_signals

    result = ""
    for type_id in consolidated_signals["type_ids"]:
        if signal_name in consolidated_signals["type_ids"][type_id]:
            result = result + str(fill_str)

        result = result + ","

    return result


def analyze_information_models(model_list, output_path=None):
    global consolidated_signals
    global consolidated_fp

    if not isinstance(model_list, list):
        logging.error("invalid model list")
        return

    if output_path and os.path.exists(output_path) is False:
        os.makedirs(output_path, exist_ok=True)

    output_type = None
    consolidated_file_name = None
    if parsed_values.uml_diagram:
        output_type = "uml"
    elif parsed_values.markdown:
        output_type = "markdown"
        consolidated_file_name = "index.md"
    elif parsed_values.csv:
        output_type = "csv"
        consolidated_file_name = "consolidated_data.csv"

    consolidated_fp = open(output_path + os.sep + consolidated_file_name, "w") if consolidated_file_name else None

    if consolidated_fp:
        if output_type == "markdown":
            consolidated_fp.write("# Model Index\n\n")
            consolidated_fp.write("| Type Id | Name | Description |\n")
            consolidated_fp.write("| :--- | :---: | :--- |\n")

        elif output_type == "csv":
            if consolidated_fp:
                consolidated_fp.write(f'typeID,variableName,description,readAddress,value,bitfield,regType,dataType,'
                                      f'registryCount,min,max,unit,scale,pollingType,pollingTypes,targetDataType,'
                                      f'invalidValue,tpName,forceValue\n')

    for type_id in model_list:
        logging.info("analysing " + type_id)

        if output_path:
            temp_file_path = output_path + os.sep + ".temp.txt"
            temp_fp = open(temp_file_path, "w")

            output_file_path = ""
            if output_type == "uml":
                output_file_path = output_path + os.sep + type_id + ".txt"

                temp_fp.write("@startuml" + "\n")
                temp_fp.write("title " + type_id + "\n")

            elif output_type == "markdown":
                output_file_path = output_path + os.sep + type_id + ".md"

                if consolidated_fp:
                    consolidated_fp.write(f'| [{type_id}]({type_id}.html) | {model_fetch_list[type_id].get("name")} '
                                          f'| {model_fetch_list[type_id].get("description")} |\n')

            elif output_type == "csv":
                output_file_path = output_path + os.sep + type_id + ".csv"

            type_id_signals.clear()

            add_type_id_info(type_id=type_id, output_fp=temp_fp, output_type=output_type)

            analyze_type_id(model_key=type_id, output_fp=temp_fp, output_type=output_type)

            consolidated_signals["type_ids"][type_id] = []
            for item_key in type_id_signals:
                if item_key not in consolidated_signals["type_ids"][type_id]:
                    consolidated_signals["type_ids"][type_id].append(item_key)

                if item_key not in consolidated_signals["signals"]:
                    consolidated_signals["signals"][item_key] = type_id_signals[item_key]
                else:
                    for sub_item in type_id_signals[item_key]:
                        if type_id_signals[item_key][sub_item]:
                            consolidated_signals["signals"][item_key][sub_item] = type_id_signals[item_key][sub_item]

            if output_type == "uml":
                temp_fp.write("@enduml" + "\n")

            elif output_type == "markdown":
                if os.path.exists(f"{output_path}/png/{type_id}.png"):
                    temp_fp.write("\n\n## Class View:\n")
                    temp_fp.write(f'![{type_id}]({type_id}.png)')

            temp_fp.close()

            if output_type == "uml":
                finalize_uml_file(src_file_path=temp_file_path, dst_file_path=output_file_path)
            else:
                replace_if_modified(src_file_path=temp_file_path, dst_file_path=output_file_path)

            if os.path.exists(temp_file_path):
                os.remove(temp_file_path)
        else:
            analyze_type_id(model_key=type_id, output_fp=None)

    if consolidated_fp:
        consolidated_fp.close()

    create_output_files(output_path=analysis_output_path, output_type=output_type)


def create_output_files(output_path, output_type=None):
    if not output_type:
        return

    file_converter = None

    if output_type == "uml":
        try:
            import plantuml
        except ImportError as err:
            logging.error("error importing planuml module")
            logging.error(err)
            return

        file_converter = plantuml.PlantUML(url="http://www.plantuml.com/plantuml/img/")
    elif output_type == "markdown":
        try:
            import pypandoc
        except ImportError as err:
            logging.error("error importing pypandoc module")
            logging.error(err)
            return

        file_converter = pypandoc

    if output_path and os.path.exists(output_path) is False:
        logging.error(output_path + "does not exist.")
        return

    query_str = ""
    if output_type == "uml":
        query_str = output_path + os.sep + "*.txt"
    elif output_type == "markdown":
        query_str = output_path + os.sep + "*.md"

    logging.debug(query_str)

    input_file_list = glob.glob(query_str)

    if output_type == "uml":
        output_path = output_path + os.sep + "png"
    elif output_type == "markdown":
        output_path = output_path + os.sep + "html"

    os.makedirs(output_path, exist_ok=True)

    for txt_file in input_file_list:
        base_file_name = os.path.splitext(os.path.basename(txt_file))[0]
        output_file_name = "no_name.ext"
        if output_type == "uml":
            output_file_name = output_path + os.sep + base_file_name + ".png"
        elif output_type == "markdown":
            output_file_name = output_path + os.sep + base_file_name + ".html"

        try:
            create_output_file = True
            if parsed_values.force_output is False and os.path.exists(output_file_name):
                if os.path.getmtime(txt_file) < os.path.getmtime(output_file_name):
                    create_output_file = False
                    logging.info("output file already up-to-date. skipping " + os.path.basename(txt_file))

            if create_output_file is True:
                logging.info("creating output file for " + os.path.basename(txt_file))

                if output_type == "uml" and file_converter:
                    file_converter.processes_file(txt_file, outfile=output_file_name)
                elif output_type == "markdown" and file_converter:
                    file_converter.convert_file(txt_file, "html", outputfile=output_file_name,
                                                extra_args=[
                                                    "--css=pandoc.css",
                                                    "--self-contained",
                                                    f'--resource-path={os.path.abspath(output_path + "/../png")}'
                                                ])

        except Exception as err:
            logging.error(f"error occurred while creating output file from {txt_file} to {output_file_name}")
            logging.error(err)


def parse_args():
    import argparse

    parser = argparse.ArgumentParser(description='ABB ability info model analyzer')

    parser.add_argument(dest="root_dir", metavar="<model root directory>", type=str, help='model root directory path')
    parser.add_argument('-ff', "--file-filter", dest="file_filter", type=str, help='file filter ex:*.json')
    parser.add_argument('-fl', "--file-list", dest="file_list", type=str, help='file list')

    parser.add_argument('-b', "--variables", dest="add_variables", action="store_true",
                        help='add variables to output file')
    parser.add_argument('-p', "--properties", dest="add_properties", action="store_true",
                        help='add properties to output file')
    parser.add_argument('-bt', "--base-types", dest="base_types", type=int, default=1,
                        help='add base types up to given levels to output file. '
                             'ex:'
                             '--base-types=2.   [to add up to 2 levels]. '
                             '--base-types=-1.  [to add for all levels]')
    parser.add_argument('-rl', "--related-models", dest="related_models", type=int, default=1,
                        help='add related models up to given levels to output file. '
                             'ex:'
                             '--related-models=2.   [to add up to 2 levels]. '
                             '--related-models=-1.  [to add for all levels]')
    parser.add_argument('-rf', "--reference-models", dest="reference_models", type=int, default=1,
                        help='add reference models up to given levels to output file. '
                             'ex:'
                             '--reference-models=2.   [to add up to 2 levels]. '
                             '--reference-models=-1.  [to add for all levels]')

    parser.add_argument('-r', "--reformat", dest="file_reformat", action="store_true",
                        help='reformat json files')

    parser.add_argument('-iw', "--ignore-warning", dest="ignore_warning",
                        action="store_true", default=False, help='ignores warning')

    parser.add_argument('-ia', "--ignore-attribute-check", dest="ignore_attribute_check",
                        action="store_true", default=False, help='ignores checking attribute')

    parser.add_argument('-u', "--uml-diagram", dest="uml_diagram", action="store_true",
                        help='create plantuml class diagram')

    parser.add_argument('-m', "--markdown", dest="markdown", action="store_true",
                        help='create markdown files')

    parser.add_argument('-c', "--csv", dest="csv", action="store_true",
                        help='comma seperated file')

    parser.add_argument('-f', "--force", dest="force_output", action="store_true",
                        help='force output files creation')

    parser.add_argument('-v', "--verbose", dest="verbose", action="store_true", help='verbosity')

    return parser.parse_args()


if __name__ == '__main__':
    parsed_values = parse_args()

    try:
        model_fetch_list = dict()
        if parsed_values.verbose:
            logging.basicConfig(level=logging.DEBUG, format="%(asctime)s: %(levelname)s: %(message)s")
        else:
            logging.basicConfig(level=logging.INFO, format="%(asctime)s: %(levelname)s: %(message)s")

        if not parsed_values.root_dir or not os.path.exists(parsed_values.root_dir):
            logging.exception("invalid root directory")
            exit(1)

        root_abs_path = os.path.abspath(parsed_values.root_dir)

        if parsed_values.base_types == -1:
            parsed_values.base_types = 100

        if parsed_values.related_models == -1:
            parsed_values.related_models = 100

        if parsed_values.reference_models == -1:
            parsed_values.reference_models = 100

        if parsed_values.csv:
            parsed_values.add_properties = True
            parsed_values.add_variables = True

        file_query_str = root_abs_path + os.sep + "**\\*.json"
        fetch_list = glob.glob(file_query_str, recursive=True)
        logging.info("fetch file count = " + str(len(fetch_list)))

        if len(fetch_list) == 0:
            logging.error("0 file found for fetching. quiting...")
            exit(1)

        if parsed_values.file_list:
            if ".lst" in parsed_values.file_list:
                with open(parsed_values.file_list, "r") as fp_file_list:
                    while True:
                        line = fp_file_list.readline()
                        if not line:
                            break

                        for file_abs_path in fetch_list:
                            if line.strip("\n ") in file_abs_path:
                                analyse_list.append(file_abs_path)
            else:
                for file_name in parsed_values.file_list.split(","):
                    file_path = root_abs_path + os.sep + file_name.strip()
                    if file_path not in fetch_list:
                        analyse_list.append(file_path)
        else:
            if not parsed_values.file_filter:
                parsed_values.file_filter = "*.json"

            file_query_str = root_abs_path + os.sep + "**\\" + parsed_values.file_filter
            analyse_list = glob.glob(file_query_str, recursive=True)

        logging.info("analyse file count = " + str(len(analyse_list)))

        if len(analyse_list) == 0:
            logging.error("0 file found for analysis. quiting...")
            exit(1)

        fetch_all_models()

        if parsed_values.file_reformat is True:
            format_all_models()

        model_analyse_list.sort()

        analysis_output_path = os.path.abspath(root_abs_path + os.sep + "output")

        analyze_information_models(model_list=model_analyse_list, output_path=analysis_output_path)

        err_file_log_fp = open(root_abs_path + os.sep + "warnings.log", "w")

        for warn_str in warning_list:
            logging.warning(warn_str)
            err_file_log_fp.write("warning: " + warn_str + "\n")

        for error_str in error_list:
            logging.error(error_str)
            err_file_log_fp.write("error: " + error_str + "\n")

        err_file_log_fp.close()

    except:
        logging.exception("exception")
