# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from openpyxl import load_workbook
import logging
import json
import os


def value_list_from_cell_range(cell_range):
    if not isinstance(cell_range, tuple):
        return None

    value_list = list()
    if isinstance(cell_range[0], tuple):
        for row_item in cell_range:
            for cell_item in row_item:
                value_list.append(cell_item.value)

    return value_list


def config_model_items():
    info_model = {
        "model": "abb.ability.configuration",
        "typeId": "",
        "version": "",
        "name": "",
        "properties": {},
        "attributes": {}
    }
    return info_model


def device_model_items():
    info_model = {
        "model": "abb.ability.device",
        "typeId": "",
        "version": "",
        "name": "",
        "properties": {},
        "attributes": {},
        "variables": {},
        "methods": {},
        "relatedModels": {}
    }
    return info_model


def default_config_attributes():
    def_attributes = {
        "scale": {
            "dataType": "number",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean"
            ]
        },
        "offset": {
            "dataType": "number",
            "appliesTo": [
                "array",
                "integer",
                "number"
            ]
        },
        "readAddress": {
            "dataType": "integer",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "bitfield": {
            "dataType": "integer",
            "appliesTo": [
                "boolean",
                "integer"
            ]
        },
        "disabled": {
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ],
            "dataType": "boolean"
        },
        "mask": {
            "dataType": "integer",
            "appliesTo": [
                "integer"
            ]
        },
        "forceValue": {
            "dataType": "boolean",
            "appliesTo": [
                "integer"
            ]
        },
        "regType": {
            "dataType": "string",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ],
            "enum": [
                "INPUT REGISTER",
                "RESERVED HOLDING REGISTER",
                "HOLDING REGISTER",
                "OUTPUT REGISTER",
                "RESERVED REGISTER",
                "RESERVED HOLDING",
                "HOLDING",
                "OUTPUT",
                "RESERVED",
                "INPUT",
                "DISCRETE",
                "COILS"
            ]
        },
        "registryCount": {
            "dataType": "integer",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "pollingType": {
            "dataType": "string",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ],
            "enum": [
                "fast",
                "slow",
                "once"
            ]
        },
        "pollingTypes": {
            "dataType": "string",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "targetDataType": {
            "dataType": "string",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "readRuleName": {
            "dataType": "string",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "emit": {
            "dataType": "boolean",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "sendOnReconnection": {
            "dataType": "boolean",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "readForNTimes": {
            "dataType": "integer",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "readInterval": {
            "dataType": "string",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        },
        "littleEndian": {
            "dataType": "boolean",
            "appliesTo": [
                "array",
                "integer",
                "number"
            ]
        },
        "invalidValue": {
            "dataType": "number",
            "appliesTo": [
                "array",
                "integer",
                "number"
            ]
        },
        "tpName": {
          "appliesTo": [
            "array",
            "integer",
            "number"
          ],
          "dataType": "string"
        },
        "writeAddress": {
          "appliesTo": [
            "array",
            "integer",
            "number",
            "boolean",
            "string"
          ],
          "dataType": "integer"
        }
    }
    return def_attributes


def default_device_attributes():
    def_attributes = {
        "minItems": {
            "dataType": "number",
            "appliesTo": [
                "array"
            ]
        },
        "maxItems": {
            "dataType": "number",
            "appliesTo": [
                "array"
            ]
        },
        "disabled": {
            "dataType": "boolean",
            "appliesTo": [
                "array",
                "integer",
                "number",
                "boolean",
                "string"
            ]
        }
    }
    return def_attributes


config_info_model = dict()
device_info_model = dict()
working_dir = "."


def value_of(cell_value: str):
    cell_value = cell_value.replace("\n", ",")
    if "," in cell_value:
        value_list = cell_value.split(",")
        cell_value = list()
        for value in value_list:
            cell_value.append(value.strip())
    else:
        cell_value = cell_value.strip()

    return cell_value


def sort_dictionary(assorted_dict: dict):
    sorted_dict = dict()

    if parsed_values.skip_attribute_sort is True:
        preferred_order = ["dataType","unit", "min", "max", "description", "value", "readAddress", "regType", "registryCount",
                           "scale", "pollingType", "pollingTypes", "targetDataType", "forceValue", "disabled"]
        dict_key_list = list(assorted_dict.keys())
        # append preferred key list first
        sorted_key_list = [item for item in preferred_order if item in dict_key_list]

        # append keys not in preferred list
        sorted_key_list += [item for item in dict_key_list if item not in preferred_order]

    else:
        sorted_key_list = sorted(assorted_dict.keys())

    for key in sorted_key_list:
        sorted_dict[key] = assorted_dict[key]

    return sorted_dict


def extract_prop_info(work_sheet):
    global config_info_model
    global device_info_model

    if config_info_model is None or device_info_model is None:
        raise Exception("Null model reference")

    if work_sheet is None:
        raise Exception("Null or invalid work sheet reference passed")

    min_col_number = 0
    max_col_number = 0
    min_col_letter = "A"
    max_col_letter = None
    # key_list = value_list_from_cell_range(prop_ws[f"A1:AZ1"][0])
    # if "name" in key_list:
    #     min_col_number = key_list.index("name") + 1

    break_for_loop = False
    row_ref = 3
    for col in work_sheet.iter_cols(min_row=row_ref, min_col=min_col_number, max_col=100, max_row=row_ref):
        for cell in col:
            if cell.value is None:
                break_for_loop = True
            else:
                max_col_letter = cell.column_letter
                max_col_number = cell.column
                logging.debug(cell.coordinate + " : " + cell.value)

        if break_for_loop is True:
            break

    if max_col_letter is None:
        return False

    logging.info(f"max. column letter = {max_col_letter}")
    logging.debug(f"max. column number = {max_col_number}")

    config_field_list = value_list_from_cell_range(work_sheet[f"{min_col_letter}1:{max_col_letter}1"])
    device_field_list = value_list_from_cell_range(work_sheet[f"{min_col_letter}2:{max_col_letter}2"])
    key_list = value_list_from_cell_range(work_sheet[f"{min_col_letter}3:{max_col_letter}3"])
    header_list = value_list_from_cell_range(work_sheet[f"{min_col_letter}4:{max_col_letter}4"])

    logging.debug(f"config.field list = {config_field_list}")
    logging.debug(f"device.field list = {device_field_list}")
    logging.info(f"key list = {key_list}")
    logging.info(f"header list = {header_list}")

    max_rows = 10000
    break_for_loop = False
    row_count = 5
    while row_count < max_rows and break_for_loop is False:
        row_list = value_list_from_cell_range(work_sheet[f"{min_col_letter}{row_count}:{max_col_letter}{row_count}"])
        row_count = row_count + 1

        config_cat_name = None
        device_cat_name = None
        config_item = None
        device_item = None
        item_key_name = None
        col_count = 0
        none_count = 0
        for key in key_list:
            if col_count < len(row_list) and row_list[col_count] is not None and \
                    len(str(row_list[col_count])) > 0:
                if key == "name":
                    item_key_name = row_list[col_count].replace(" ", "")

                    if config_cat_name is not None:
                        config_item = dict()

                    if device_cat_name is not None:
                        device_item = dict()

                elif key == "config":
                    config_cat_name = row_list[col_count]
                    if config_cat_name not in config_info_model:
                        config_info_model[config_cat_name] = {}

                elif key == "device":
                    device_cat_name = row_list[col_count]
                    if device_cat_name not in device_info_model:
                        device_info_model[device_cat_name] = {}

                else:
                    if config_item is not None and col_count < len(config_field_list) and \
                            config_field_list[col_count] is not None and \
                            any(field_val.casefold() == config_field_list[col_count].casefold()
                                for field_val in ["yes", "true"]):

                        if key == "readAddress":
                            config_item["value"] = row_list[col_count]
                        elif key == "dataType" and "value" in config_item and row_list[col_count] == "number":
                            config_item["value"] = float(config_item["value"])

                        if (key == "scale") or \
                                (key in ["value", "min", "max"] and config_item.get("dataType") == "number"):
                            config_item[key] = float(row_list[col_count])
                        else:
                            config_item[key] = row_list[col_count]

                    if device_item is not None and col_count < len(device_field_list) and \
                            device_field_list[col_count] is not None and \
                            any(field_val.casefold() == device_field_list[col_count].casefold()
                                for field_val in ["yes", "true"]):

                        if key in ["min", "max"]:
                            device_item[key] = float(row_list[col_count])
                        else:
                            device_item[key] = row_list[col_count]

            else:
                none_count = none_count + 1

            col_count = col_count + 1

        if col_count == none_count:
            break_for_loop = True
            continue

        logging.debug("row=" + str(row_list))
        logging.debug("config=" + str(config_item))
        logging.debug("device=" + str(device_item))

        if item_key_name is not None:
            if config_item is not None and config_info_model is not None and config_cat_name:
                config_item = sort_dictionary(config_item)

                if item_key_name == "onceConfig":
                    if "readForNTimes" not in config_item:
                        config_item["readForNTimes"] = 5

                    if "readInterval" not in config_item:
                        config_item["readInterval"] = "00:01:00"

                if config_cat_name not in config_info_model:
                    config_info_model[config_cat_name] = dict()

                if item_key_name in config_info_model[config_cat_name]:
                    config_info_model[config_cat_name].update({item_key_name: config_item})
                else:
                    config_info_model[config_cat_name][item_key_name] = config_item

            if device_item is not None and device_info_model is not None and device_cat_name:
                device_item = sort_dictionary(device_item)

                if device_cat_name not in device_info_model:
                    device_info_model[device_cat_name] = dict()

                if item_key_name in device_info_model[device_cat_name]:
                    device_info_model[device_cat_name].update({item_key_name: device_item})
                else:
                    device_info_model[device_cat_name][item_key_name] = device_item


def extract_header_info(work_sheet):
    global config_info_model
    global device_info_model

    if config_info_model is None or device_info_model is None:
        raise Exception("Null model reference")

    if work_sheet is None:
        raise Exception("Null or invalid work sheet reference passed")

    min_row_number = 1
    max_row_number = None

    break_for_loop = False
    column_ref = 1
    for row in work_sheet.iter_rows(min_row=min_row_number, min_col=column_ref,
                                    max_row=100, max_col=column_ref):
        for cell in row:
            if cell.value is None:
                break_for_loop = True
            else:
                max_row_number = cell.row
                logging.debug(cell.coordinate + " : " + cell.value)

        if break_for_loop is True:
            break

    if max_row_number is None:
        return False

    key_list = value_list_from_cell_range(work_sheet[f"A{min_row_number}:A{max_row_number}"])
    header_list = value_list_from_cell_range(work_sheet[f"B{min_row_number}:B{max_row_number}"])
    config_info_list = value_list_from_cell_range(work_sheet[f"C{min_row_number}:C{max_row_number}"])
    device_info_list = value_list_from_cell_range(work_sheet[f"D{min_row_number}:D{max_row_number}"])

    logging.info(f"key list = {key_list}")
    logging.info(f"header list = {header_list}")
    logging.info(f"config header list = {config_info_list}")
    logging.info(f"device header list = {device_info_list}")

    row_count = 0

    for key in key_list:
        config_val = None
        device_val = None

        if any(key == key_item for key_item in ["model", "typeId", "version", "name", "baseTypes"]):
            if row_count < len(config_info_list) and config_info_list[row_count] is not None:
                config_val = value_of(config_info_list[row_count])

            if row_count < len(device_info_list) and device_info_list[row_count] is not None:
                device_val = value_of(device_info_list[row_count])

            if key == "baseTypes":
                if config_val and isinstance(config_val, list) is False:
                    config_val = [config_val]

                if device_val and isinstance(device_val, list) is False:
                    device_val = [device_val]

            if config_val:
                config_info_model[key] = config_val

            if device_val:
                device_info_model[key] = device_val

        row_count = row_count + 1


def append_other_fields():
    # update config model
    if "properties" not in config_info_model:
        config_info_model["properties"] = {}

    if "attributes" not in config_info_model:
        if parsed_values.skip_attributes is False:
            config_info_model["attributes"] = default_config_attributes()
        else:
            config_info_model["attributes"] = {}

    # update device model
    for device_list_fields in ["properties", "variables"]:
        if device_list_fields not in device_info_model:
            device_info_model[device_list_fields] = {}

    if "attributes" not in device_info_model:
        if parsed_values.skip_attributes is False:
            device_info_model["attributes"] = default_device_attributes()
        else:
            device_info_model["attributes"] = {}

    if "methods" not in device_info_model:
        device_info_model["methods"] = {}

    if "relatedModels" not in device_info_model:
        device_info_model["relatedModels"] = {}

    device_info_model["relatedModels"] = {
        "abb.ability.configuration": {
            "type": f'{config_info_model.get("typeId")}@{config_info_model.get("version").split(".")[0]}'
        }
    }


def extract_model_info(input_file: str,
                       config_input: str = None, device_input: str = None,
                       config_output: str = None, device_output: str = None):
    global config_info_model
    global device_info_model

    wb = load_workbook(input_file)
    logging.debug(wb.sheetnames)

    if config_input:
        with open(config_input, "r") as config_input_fp:
            json_content = str()
            while True:
                line = config_input_fp.readline()
                if not line:
                    break
                if "//" not in line:
                    json_content += line.strip()
            config_info_model = json.loads(json_content)

    if device_input:
        with open(device_input, "r") as device_input_fp:
            json_content = str()
            while True:
                line = device_input_fp.readline()
                if not line:
                    break
                if "//" not in line:
                    json_content += line.strip()
            device_info_model = json.loads(json_content)

    extract_header_info(wb["header"] if "header" in wb.sheetnames else None)
    extract_prop_info(wb["variables"] if "variables" in wb.sheetnames else None)
    append_other_fields()

    if config_output is None:
        config_output = working_dir + os.sep + "configuration"

    slave_id = 0
    if "properties" in config_info_model and "slaveID" in config_info_model["properties"]:
        slave_id = config_info_model["properties"]["slaveID"].get("value", 0)

    config_output += os.sep
    if slave_id:
        config_output += f'{slave_id}_'

    config_output += config_info_model.get("typeId", "configuration") + ".json"
    if os.path.exists(config_output):
        logging.warning(f"config output file ({config_output}) exists and it will be over written")

    os.makedirs(os.path.dirname(config_output), exist_ok=True)

    with open(config_output, "w") as config_output_fp:
        json.dump(config_info_model, config_output_fp, indent=2, sort_keys=False, ensure_ascii=False)
        logging.info("storing config info model data to " + config_output)

    if device_output is None:
        device_output = working_dir + os.sep + "device"

    device_output += os.sep
    if slave_id:
        device_output += f'{slave_id}_'

    device_output += device_info_model.get("typeId", "device") + ".json"
    if os.path.exists(device_output):
        logging.warning(f"device output file ({device_output}) exists and it will be over written")

    os.makedirs(os.path.dirname(device_output), exist_ok=True)

    with open(device_output, "w") as device_output_fp:
        json.dump(device_info_model, device_output_fp, indent=2, sort_keys=False, ensure_ascii=False)
        logging.info("storing device info model data to " + device_output)


def parse_args():
    import argparse

    parser = argparse.ArgumentParser(description='ABB ability info model data extractor')

    parser.add_argument(dest="input_file", metavar="<input xlsx file>",
                        type=str, help='input excel file path. ex: mysite_care_0.1.xlsx')

    parser.add_argument('-ci', "--config-input", dest="config_input", help='configuration input file')
    parser.add_argument('-co', "--config-output", dest="config_output", help='configuration output file')

    parser.add_argument('-di', "--device-input", dest="device_input", help='device input file')
    parser.add_argument('-do', "--device-output", dest="device_output", help='device output file')

    parser.add_argument('-sa', "--skip-attributes", dest="skip_attributes", help='skips attributes sections',
                        action="store_true", default=False)
    parser.add_argument('-ss', "--skip-attribute-sorting", dest="skip_attribute_sort",
                        help='skips sorting of attributes', action="store_true", default=False)

    parser.add_argument('-v', "--verbose", dest="verbose", action="store_true", help='verbosity')

    return parser.parse_args()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    try:
        parsed_values = parse_args()
        if parsed_values.verbose:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

        logging.debug("input_file= " + parsed_values.input_file)
        arg_input_file = os.path.abspath(parsed_values.input_file)

        if not os.path.exists(arg_input_file):
            raise FileNotFoundError(f"input file does not exist ({arg_input_file})")

        if parsed_values.config_input:
            logging.info("config_input= " + parsed_values.config_input)
            arg_config_input = os.path.abspath(parsed_values.config_input)
            if not os.path.exists(arg_config_input):
                raise FileNotFoundError(f"config input file does not exist ({arg_config_input})")

        if parsed_values.config_output:
            logging.info("config_output= " + parsed_values.config_output)

        if parsed_values.device_input:
            logging.info("device_input= " + parsed_values.device_input)
            arg_device_input = os.path.abspath(parsed_values.device_input)
            if not os.path.exists(arg_device_input):
                raise FileNotFoundError(f"device input file does not exist ({arg_device_input})")

        if parsed_values.device_output:
            logging.info("device_output= " + parsed_values.device_output)

        working_dir = os.path.abspath(os.path.dirname(arg_input_file))
        logging.info("Working Dir= " + working_dir)
        logging.info("Starting data extraction from " + arg_input_file)

        extract_model_info(input_file=arg_input_file,
                           config_input=parsed_values.config_input, device_input=parsed_values.device_input,
                           config_output=parsed_values.config_output, device_output=parsed_values.device_output)

        logging.info("Data extraction completed")

    except:
        logging.exception("exception")
